# Hardware Blockchain
A distributed data system co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed on [IPFS](https://ipfs.io/), to be used with a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://gitlab.com/breathinggames/bg_blockchain/wikis/home).**

Watch the [presentation](https://docs.google.com/presentation/d/1rSUWl1BfzSTQlEqNgGqxEwqLYMiFl9w2ns8sfF143eI).


## In short
A privacy and reward model for the public sharing of health data.


## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net