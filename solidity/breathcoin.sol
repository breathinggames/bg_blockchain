/* 
This creates a digital cryptocurrency for the Breating Games project.
The token will be controlled by another contract , the BREATHmint (pun intended!)

That contract has the powers to create new tokens out of thin air,
and freeze and unfreeze accounts, preventing them from sending these tokens.
This ABI for this contract will be accessed through a webservice that a game can
call once a breathing therapy session has been completed. The data will be stored on
IPFS and the hash sent through another contract.

This contract is intended for educational purposes, you are fully responsible 
for compliance with present or future regulations of finance, communications 
and the universal rights of digital beings.

For more information, please refer to <http://breathinggames.net>
*/

contract mintableBreathCoin { 
    /* Public variables of the token */
    string public name;
    string public symbol;
    uint8 public decimals;
    address public issuer;
    
    /* This creates an array with all balances */
    mapping (address => uint) public balanceOf;
    mapping (address => bool) public frozenAccount;
    
    /* This generates a public event on the blockchain that will notify clients */
    event Transfer(address indexed from, address indexed to, uint256 value);
    event FrozenFunds(address target, bool frozen);
    
    /* Initializes contract with initial supply tokens to the creator of the contract */
    function mintableBreathCoin(uint256 initialSupply, uint8 decimalUnits, string tokenSymbol, address breathMint) {
        /* if supply not given then generate 1 million of the smallest unit of the token */
        if (initialSupply == 0) initialSupply = 1000000;
        
        if(breathMint == 0) breathMint = msg.sender;
        issuer = breathMint;
        
        /* Unless you add other functions these variables will never change */
        balanceOf[breathMint] = initialSupply;
        name = "breathCoin";     
        symbol = tokenSymbol;
        
        /* If you want a divisible token then add the amount of decimals the base unit has  */
        decimals = decimalUnits;
    }

    /* Send coins */
    function transfer(address recipient, uint256 amount) {
        /* if the sender doenst have enough balance then stop */
        if (balanceOf[msg.sender] < amount  /* has enough funds? */
            || balanceOf[recipient] + amount < balanceOf[recipient] /* Checks for overflows */
            || frozenAccount[msg.sender])  /* is frozen? */
            throw;
        
        /* Add and subtract new balances */
        balanceOf[msg.sender] -= amount;
        balanceOf[recipient] += amount;
        
        /* Notifiy anyone listening that this transfer took place */
        Transfer(msg.sender, recipient, amount);
    }
    
    modifier onlyIssuer {
        if (msg.sender != issuer) throw;
        _
    }
    
    function mintToken(address target, uint256 mintedAmount) onlyIssuer {
        balanceOf[target] += mintedAmount;  
        
        Transfer(0, target, mintedAmount);
    }
    
    function freezeAccount(address target, bool freeze) onlyIssuer {
        frozenAccount[target] = freeze;
        
        FrozenFunds(target, freeze);
    }
}
